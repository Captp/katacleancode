export enum Activity {
    Nothing = 0,
    Work = 1,
    Eat = 2,
    Sleep = 3,
    Other = 4,
    Standby = 5
}

export class ActivityHelper {

    constructor() {}
    
    static getActivity(activity: string): Activity {
        switch (activity) {
            case Activity[Activity.Standby]:    return Activity.Standby;
            case Activity[Activity.Other]:      return Activity.Other;
            case Activity[Activity.Sleep]:      return Activity.Sleep;
            case Activity[Activity.Eat]:        return Activity.Eat;
            case Activity[Activity.Work]:       return Activity.Work;
            default:                            return Activity.Nothing;
        }
    }

    static allowedActivity(activity: string, nature: string): Activity {
        if (nature === "activity") {
            switch (activity) {
                case Activity[Activity.Other]:      return Activity.Other;
                case Activity[Activity.Sleep]:      return Activity.Sleep;
                case Activity[Activity.Eat]:        return Activity.Eat;
                case Activity[Activity.Work]:       return Activity.Work;
                default:                            return Activity.Nothing;
            }
        }
        else if (nature === "robot") {
            switch (activity) {
                case Activity[Activity.Standby]:    return Activity.Standby;
                case Activity[Activity.Other]:      return Activity.Other;
                case Activity[Activity.Work]:       return Activity.Work;
                default:                            return Activity.Nothing;
            }
        }
    }
}