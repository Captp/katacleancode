import { Activity } from "./Activity";
import { SimpleDate } from "./SimpleDate";

export abstract class Command {

    constructor(
        protected name: string,
        protected date?: SimpleDate
    ) {}

    getName(): string { return this.name; }
    getDate(): SimpleDate { return this.date; }
}

export class ActivityCommand extends Command {

    private activity: Activity;
    private start: number;
    private end: number;

    constructor(
        name: string,
        activity: Activity,
        start: number,
        end: number,
        date?: SimpleDate
    ) {
        super(name, date);
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    getActivity(): Activity { return this.activity; }
    getStart(): number { return this.start; }
    getEnd(): number { return this.end; }
    setEndToMidnight() { this.end = 24; }
}

export enum ActivityIndexer {
    Nature = 0,
    Name = 1,
    Activity = 2,
    Start = 3,
    End = 4,
    Date = 5        
}

export class TaskCommand extends Command {

    private task: string;

    constructor(
        name: string,
        task: string,
        date: SimpleDate
    ) {
        super(name, date);
        this.task = task;
    }

    getTask(): string { return this.task; }
}

export enum TaskIndexer {
    Name = 1,
    Task = 2,
    Date = 3
}