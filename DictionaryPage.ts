import { Activity } from "./Activity";
import { ActivityCommand, TaskCommand } from "./Command";
import { SimpleDate, SimpleDateHelper } from "./SimpleDate";
import { WorkerDay, WorkerDayHelper } from "./WorkerDay";

export class DictionaryPage {

    private date: SimpleDate;
    private workerDays: WorkerDay[];

    constructor(
        date: SimpleDate,
        pages?: WorkerDay[]
    ) {
        this.date = date;
        if (pages) this.workerDays = pages;
        else this.workerDays = [];
    }

    getDate(): SimpleDate { return this.date; }

    getContent(): WorkerDay[] { return this.workerDays; }

    getWorkerDay(name: string): WorkerDay {
        let workerDay: WorkerDay;

        for (let wDay of this.workerDays) {
            if (wDay.getName() === name) workerDay = wDay;
        }

        return workerDay || undefined;
    }
}

export abstract class DictionaryPageHelper {

    static sort(pageA: DictionaryPage, pageB: DictionaryPage): number {
        return SimpleDateHelper.compare(pageA.getDate(), pageB.getDate());
    }

    static commandToDictionaryPage(cmd: ActivityCommand | TaskCommand): DictionaryPage {
        let date = cmd.getDate() || SimpleDateHelper.defaultDate;
        let workerDay = WorkerDayHelper.commandToWorkerDay(cmd);
        let content = [];
        content.push(workerDay);
        
        return new DictionaryPage(date, content);
    }
}