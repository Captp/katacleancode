import { Command, ActivityCommand, TaskCommand } from "./Command";
import { CommandBuilder } from "./CommandBuilder";
import { DictionaryPage, DictionaryPageHelper } from "./DictionaryPage";
import { SimpleDate, SimpleDateHelper } from "./SimpleDate";
import { WorkerDay, WorkerDayHelper } from "./WorkerDay";

import fs = require("fs");

export class WorkerDictionary {
    
    public dict: DictionaryPage[];

    constructor() {
        this.dict = [];
    }

    toCSV() {
        let buffer: string[] = [];
        this.dict.forEach(dictPage => dictPage.getContent().forEach(workDay => {
            let date = SimpleDateHelper.toStringDate(dictPage.getDate());
            buffer.push(date + ";" + workDay.toString());
        }));
        fs.writeFileSync("foo.csv", buffer.join("\n"));
    }

    executeList(commands: (TaskCommand | ActivityCommand)[]): number {
        let executions = 0;
        
        commands.forEach(command => executions += this.execute(command));

        return executions;
    }

    // 1 for inserts, 0 for updates
    private execute(command: TaskCommand | ActivityCommand): number {
        let executions = 0;
        let commandList = [];

        // If it is an activity on two days
        if (command instanceof ActivityCommand && command.getStart() >= command.getEnd()) {
            // Tomorrow
            commandList.push(
                CommandBuilder.duplicateWithLeadingActivities(command, 0, command.getEnd())
            );
            // Today
            command.setEndToMidnight();
            commandList.push(command);
        }
        else {
            commandList.push(command);
        }

        commandList.forEach(command => executions += this.executeCommand(command));
        
        return executions;
    }

    private executeCommand(command: TaskCommand | ActivityCommand): number {
        let date = command.getDate();
        let pageIndex = this.dict.findIndex(
            dictPage => SimpleDateHelper.equals(dictPage.getDate(), date)
        );

        if (pageIndex === -1) {
            this.dict.push(DictionaryPageHelper.commandToDictionaryPage(command));
            this.sort();
        }
        else {
            let workerDay = this.dict[pageIndex].getWorkerDay(command.getName());

            if (!workerDay) {
                this.dict[pageIndex].getContent()
                    .push(WorkerDayHelper.commandToWorkerDay(command));
            }
            else if (command instanceof ActivityCommand) {
                this.dict[pageIndex]
                    .getWorkerDay(command.getName())
                    .setActivities(
                        command.getActivity(),
                        command.getStart(),
                        command.getEnd()
                    );
            }
            else if (command instanceof TaskCommand) {
                this.dict[pageIndex]
                    .getWorkerDay(command.getName())
                    .setTask(command.getTask());

                // ToDo : find the task before and update all tasks in-between
            }
        }

        return 1;
    }

    private sort() {
        this.dict.sort(DictionaryPageHelper.sort);
    }
}
