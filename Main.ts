import { CCKata } from "./CCKata";

class Main {
    
    public static run(): number {
/*
        let kataInput: string[] = [
            "Peter,Work,9,12",
            "Peter,Eat,12,13",
            "Peter,Work,13,17",
            "Peter,Other,17,22",
            "Peter,Sleep,22,8",
            "Paul,Work,10,16,2016-12-24",
            "Peter,Task1,2016-12-24",
            "Paul,Task2,2016-12-24"
        ];

        let myInput: string[] = [
            "activity,Peter,Other,0,24",
            "activity,Peter,Sleep,0,24,2016-10-1",
            "activity,Peter,Eat,0,2,2016-10-2",
            "activity,Peter,Work,2,24,2016-10-2",
            "robot,R2D2,Standby,0,5,2016-10-2",
            "robot,R2D2,Work,5,24,2016-10-2",
            "activity,Peter,Sleep,0,17,2016-10-3",
            "activity,Peter,Other,17,9,2016-10-3",
            "activity,Peter,Work,9,19,2016-10-4",
            "activity,Peter,Sleep,19,6,2016-10-4",
            "activity,Peter,Task_1,2016-10-1",
            "activity,Peter,Task_2,2016-10-4",
            "robot,R2D2,Task_3,2016-10-2",
            "robot,R2D2,Task_0,2016-10-7"
        ];
*/
        let processFile = "example.json"; // Use undefined to process only one command
        let uniqueCommand = "activity,Peter,Sleep,0,17,2016-10-3";

        let kata = new CCKata();
        kata.process(processFile, uniqueCommand);

        return 0;
    }
}

Main.run();