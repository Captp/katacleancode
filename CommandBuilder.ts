import { Activity, ActivityHelper } from "./Activity";
import { SimpleDateHelper } from "./SimpleDate";
import {
    Command,
    ActivityCommand, ActivityIndexer,
    TaskCommand, TaskIndexer
} from "./Command";

import fs = require("fs");

export abstract class CommandBuilder {

    static duplicateWithLeadingActivities(
        cmd: ActivityCommand,
        start: number,
        end: number
    ): ActivityCommand {
        return new ActivityCommand(
            cmd.getName(),
            cmd.getActivity(),
            start,
            end,
            SimpleDateHelper.dayAfter(cmd.getDate())
        );
    }

    static buildFromFile(filename: string): (TaskCommand | ActivityCommand)[] {
        let content = JSON.parse(fs.readFileSync(filename, "utf8"));
        let result = [];

        for (let index in content) {
            content[index].forEach(
                // We add fake comma to fake a first element
                // buildCommand() expects [0] to be person nature (human | robot)
                cmdValues => result.push(CommandBuilder.buildCommand("," + JSON.stringify(cmdValues)));
            );
        }

        return result;
    }

    static buildCommand(line: string): (TaskCommand | ActivityCommand)[] {
        let splitContent: string[] = line.split(",");
        let splitCount = splitContent.length;
        let result = [];
        
        const actionIndex = 2;
        let action: string = splitContent[actionIndex];

        ActivityHelper.getActivity(action) > Activity.Nothing
            ? result.push(CommandBuilder.createActivity(splitContent))
            : result.push(CommandBuilder.createTask(splitContent));
    
        return result;
    }

    private static createActivity(params: string[]): ActivityCommand {
        let name        = params[ActivityIndexer.Name];
        let activity    = ActivityHelper.getActivity(params[ActivityIndexer.Activity]);
        let start       = this.inDay(+params[ActivityIndexer.Start]);
        let end         = this.inDay(+params[ActivityIndexer.End]);
        let date        = params.length > ActivityIndexer.Date
                        ? SimpleDateHelper.getSimpleDate(params[ActivityIndexer.Date])
                        : undefined;

        return new ActivityCommand(name, activity, start, end, date);
    }

    private static createTask(params: string[]): TaskCommand {
        let name = params[TaskIndexer.Name];
        let task = params[TaskIndexer.Task];
        let date = SimpleDateHelper.getSimpleDate(params[TaskIndexer.Date]);

        return new TaskCommand(name, task, date);
    }

    private static inDay(val: number) {
        return val < 0 ? 0
               : val > 24 ? 24
                   : val;
    }
}