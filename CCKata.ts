import { CommandBuilder } from "./CommandBuilder";
import { WorkerDictionary } from "./WorkerDictionary";

export class CCKata {

    private dictionary: WorkerDictionary;

    constructor() { this.dictionary = new WorkerDictionary(); }

    process(fileToProcess: string, input: string): number {
    	// Build commands
    	let commands = fileToProcess === undefined
    					? CommandBuilder.buildCommand(input)
    					: CommandBuilder.buildFromFile(fileToProcess);

        // Execute them
        let executions = this.dictionary.executeList(commands);
        
        // Print CSV-formatted style dictionary in console
        this.dictionary.toCSV();
        
        return executions;
    }
}
