import { Activity } from "./Activity";
import { ActivityCommand, TaskCommand } from "./Command";

export class WorkerDay {

    private name: string;
    private activities: Activity[];
    private task: string;

    constructor(
        name: string,
        activity?: Activity,
        task?: string,
        start?: number,
        end?: number
    ) {
        this.name = name;
        this.task = task || "NOT_ASSIGNED_YET";
        this.setupActivitiesToNothing();
        if (activity && start !== undefined && end !== undefined) {
            this.setActivities(activity, start, end);
        }
    }

    toString(): string {
        return this.name + ';'
                + (this.task || "") + ';'
                + this.activities.toString() + ';'
    }

    getName(): string { return this.name; }

    setActivities(act: Activity, start: number, end: number): void {
        for (let idx = start; idx < end; idx++) { this.activities[idx] = act; }
    }

    setTask(task: string): void { this.task = task; }

    private setupActivitiesToNothing(): void {
        this.activities = [];
        this.setActivities(Activity.Nothing, 0, 24);
    }
}

export abstract class WorkerDayHelper {
    
    static commandToWorkerDay(cmd: ActivityCommand | TaskCommand): WorkerDay {

        let name: string = cmd.getName();

        if (cmd instanceof ActivityCommand) {
            let activity = cmd.getActivity();
            let start = cmd.getStart();
            let end = cmd.getEnd();

            return new WorkerDay(name, activity, undefined, start, end);
        }
        else if (cmd instanceof TaskCommand) {
            let task = cmd.getTask();

            return new WorkerDay(name, undefined, task, undefined, undefined);
        }
        else
            return undefined;
    }
}