export interface SimpleDate {
    year: number;
    month: number;
    day: number;
}

export abstract class SimpleDateHelper {

    static readonly defaultDate = { year: 2000, month: 1, day: 1 };

    static toStringDate(date: SimpleDate): string {
        return date.year + "-" + date.month + "-" + date.day;
    }
    
    static getSimpleDate(str: string): SimpleDate {
        const dateDelimiter = "-";
        let splitDate = str.split(dateDelimiter);

        return {
            year:   +splitDate[0],
            month:  +splitDate[1],
            day:    +splitDate[2]
        };
    }

    static dayAfter(date: SimpleDate): SimpleDate {
        // ToDo : Real day after func (could be another kata todo in tdd)
        return { year: date.year, month: date.month, day: date.day + 1 };
    }

    static compare(sdA: SimpleDate, sdB: SimpleDate): number {
        return sdA.year != sdB.year ? sdA.year - sdB.year           // Year difference
                : sdA.month != sdB.month ? sdA.month - sdB.month    // Month difference
                : sdA.day - sdB.day;                                // Days difference
    }

    static equals(sdA: SimpleDate, sdB: SimpleDate): boolean {
        return this.compare(sdA, sdB) === 0 ? true : false;
    }
}